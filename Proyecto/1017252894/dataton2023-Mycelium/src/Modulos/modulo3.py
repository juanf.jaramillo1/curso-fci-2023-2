import pandas as pd
import numpy as np

class Workers(object):
    """
    Esta clase se encarga de extraer la información de los trabajadores y 
    de generar un dataframe con la información de la entrega final
    
    Atributos:
    ----------
    data_demand: str
        Ruta del archivo de demanda
        
    data_workers: str
        Ruta del archivo de trabajadores
        
    cod_suc: int
        Codigo de la sucursal
        
    horario: array
        Horario de trabajo de la sucursal
        
    Metodos:
    --------
    
    Workers()
        Retorna un array donde cada elemento es un array con la siguiente informacion:
            - suc: codigo de la sucursal
            - TC: cantidad de trabajadores de TC
            - MT: cantidad de trabajadores de MT
            - maxsem: maxima de demanda en la semana
            - maxsab: maximo de demanda en el sabado
            
    EntregaFinal()
        Retorna un dataframe con el el formato de la entrega final
        
    """
    def __init__(self,data_demand,data_workers,horario=None,cod_suc=None):
        """
        Inicializa los atributos de la clase
        
        Atributos:
        ----------
        data_demand: str
            Ruta del archivo de demanda
            
        data_workers: str
            Ruta del archivo de trabajadores
            
        cod_suc: int
            Codigo de la sucursal
            
        horario: array
            Horario de trabajo de la sucursal
        """
        self.data_demand = data_demand
        self.data_workers = data_workers
        self.cod_suc = cod_suc
        self.horario = horario
        
    def Workers(self):
        """
        Este metodo retorna un array donde cada elemento es un array con la siguiente informacion:
            - suc: codigo de la sucursal
            - TC: cantidad de trabajadores de TC
            - MT: cantidad de trabajadores de MT
            - maxsem: maxima de demanda en la semana
            - maxsab: maximo de demanda en el sabado
        """
        #Cargamos los datos
        data_demand  = self.data_demand #pd.read_excel(self.data_demand)
        data_workers = self.data_workers#pd.read_excel(self.data_workers, sheet_name='workers')
        
        #Calulamos la cantidad de trabajadores de TC y MT por sucursal
        workers_info = []
        for _,suc in enumerate(data_workers["suc_cod"].unique()):
            TC = data_workers[data_workers['suc_cod'] == suc]["contrato"].value_counts()['TC']
            MT = data_workers[data_workers['suc_cod'] == suc]["contrato"].value_counts()['MT']
            
            #Calculamos el momento apartir del cual la sucursal se queda sin demanda
            #Tanto de la semana como del sabado
            i = 0
            nums = []
            num = [] 
            while True:
                if i == 245:
                    lista_numeros = data_demand[data_demand['suc_cod'] == suc]["demanda"][i:i+49].to_numpy()
                    posicion = next(i for i, num in enumerate(lista_numeros) if num == 0 and any(lista_numeros[:i]))
                    nums.append(posicion)
                    i = 0
                    break
                lista_numeros = data_demand[data_demand['suc_cod'] == suc]["demanda"][i:i+49].to_numpy()
                posicion = next(i for i, num in enumerate(lista_numeros) if num == 0 and any(lista_numeros[:i]))
                
                num.append(posicion)
        
                i += 49
            
                    
            workers_info.append([suc, TC, MT,np.max(num),nums[0]])
    
        return workers_info

    def EntregaFinal(self):
        """
        Este metodo retorna un dataframe con el el formato de la entrega final
        """
        #Cargamos los datos
        data_demand = self.data_demand #pd.read_excel(self.data_demand)
        data_workers = self.data_workers #pd.read_excel(self.data_workers, sheet_name='workers')
        
        #Separamos la fecha de la hora
        data_demand['fecha'] = data_demand['fecha_hora'].dt.date
        data_demand['hora']  = data_demand['fecha_hora'].dt.time
        
        #Eliminamos la columna fecha_hora y demanda
        data_demand = data_demand.drop(['fecha_hora','demanda'], axis=1)
        
        #Ordenamos los datos por codigo de sucursal y documento
        cod_suc = self.cod_suc
        suc_cod = data_demand[data_demand['suc_cod']  == cod_suc]
        doc_cod = data_workers[data_workers['suc_cod']== cod_suc]['documento']

        #Creamos el dataframe final 
        dataframer_apilado = []
        for i in doc_cod.to_numpy():
            suc_cod= data_demand[data_demand['suc_cod'] == cod_suc].copy()
            suc_cod['documento'] = i
            suc_cod['hora_franja'] = suc_cod.groupby('fecha').cumcount() + 30
            dataframer_apilado.append(suc_cod)  
            
        dataframer_apilado = pd.concat(dataframer_apilado)
        
        #Creamos la columna estado
        dataframer_apilado['estado'] = self.horario[5:,:-3].reshape(-1, order='F')
        
        #cramos diccionario con los estados
        dic = {0: 'Nada', 1: 'Trabaja', 2: 'Pausa Activa', 3: 'Almuerza'}
        
        #reemplazamos los numeros por los estados
        dataframer_apilado['estado'] = dataframer_apilado['estado'].map(dic)
        
        return dataframer_apilado