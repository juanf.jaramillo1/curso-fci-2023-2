import numpy as np
import matplotlib.pyplot as plt

class particula:
    """Clase para la simulación de una partícula en una caja cuántica 2D
    entradas:
    L: longitud de la caja (float)
    n: número cuántico (int)
    muestras: número de muestras para el método de Monte Carlo (int)
    n2: parámetro para el plot de la función de onda teórica (int)
    salidas:
    plot de la función de onda teórica y de la simulación de Monte Carlo
    """

    def __init__(self,L,n,muestras,n2):
        
        self.L = L
        self.n = n
        self.n2 = n2
        self.muestras = muestras

    def wave_function_2d(self,x, y, n, L):
        """Función de onda para una caja cuántica 2D"""
        psi_x = np.sqrt(2 / L) * np.sin(n * np.pi * x / L)
        psi_y = np.sqrt(2 / L) * np.sin(n * np.pi * y / L)
        return (psi_x * psi_y)**2

    def teorico(self):
        """cálculo numerico para la función de onda teórica para una caja cuántica 2D"""
        x = np.linspace(0, self.L, self.n2)
        y = np.linspace(0, self.L, self.n2)
        x, y = np.meshgrid(x, y)
        z = self.wave_function_2d(x, y, self.n, self.L)
        return x,y,z

    def plot_teorico(self):
        """"Se grafica la curva teorica calculada"""
        x,y,z = self.teorico()
        fig = plt.figure(figsize=(8, 6))
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_surface(x, y, z, rstride=5, cstride=5, cmap='viridis', edgecolor='none')
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        #ax.set_title('$|\psi|^2$ para una caja 2D con n = {}'.format(self.n))
        plt.savefig('funcion_onda_teorica.png')

    def funcion_onda_montecarlo(self):
        """Simulación de Monte Carlo para la función de onda de una particula en una caja cuántica 2D"""
        x_samples = np.random.uniform(0, self.L, self.muestras)
        y_samples = np.random.uniform(0, self.L, self.muestras)

        x = []
        y = []
        z = []
        for i in range(self.muestras):
            wave_value = self.wave_function_2d(x_samples[i], y_samples[i], self.n, self.L)
            if np.random.uniform(0, 1) < wave_value ** 2:
                x.append(x_samples[i])
                y.append(y_samples[i])
                z.append(wave_value)

            if i % 100 == 0: #Para ver el progreso de la simulación
                print('iteración: {}'.format(i))

        return x,y,z

    def plot_montecarlo(self):
        """Se grafica la simulación de Monte Carlo"""
        x,y,z = self.funcion_onda_montecarlo()
        fig = plt.figure(figsize=(8, 6))
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(x, y, z, c='b', marker='o', s=1, alpha=0.5)
        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        #ax.set_title('$|\psi|^2$ con montecarlo para una caja 2D y n = {}'.format(self.n))

        plt.savefig('funcion_onda_montecarlo.png')
    def run(self):
        self.plot_teorico()
        self.plot_montecarlo()





    