from EDO_sol import EDO
import matplotlib.pyplot as plt
import numpy as np

#NOTA 5
if __name__=="__main__":
    
    a = 0   # Limite inferior del intervalo
    b = 4   # Limite superior del intervalo
    n = 10   # Número de divisiones
    y0 = 1    # Condición inicial
    f = lambda x,y: np.cos(x)  #función de la ecuación diferencial (dy/dx = f(x,y))
    punto = 4  #Para acotar el intérvalo a un punto en específico
    
    Solver = EDO(a,b,n,y0,f,punto)
    Solver.figPlot()
