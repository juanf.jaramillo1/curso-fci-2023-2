from particle_in_magnetfield import particle_in_magnetic_field

"""
NOTA

4.2

- Buen trabajo
- No ejecuta para B=0

"""
if __name__=='__main__':
    
    E_k = 18.6   #eV
    theta = 30   #Angulo polar en Grados
    B = 0#600   #Intensidad de campo (microTesla)
    
    particle = particle_in_magnetic_field(E_k,theta,B)
    
    particle.plot_trayectory()