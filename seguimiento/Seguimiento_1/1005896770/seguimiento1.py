
import numpy as np
import matplotlib.pyplot as plt
import sys

class trayectoria:

    """
    Clase para graficar la trayectoria de un electron en presencia de
    un campo magnético.
    
    Energia: Ingrese la energía cinética del sistema en electron-voltios.
    Esta debe ser mayor a cero
    Angulo: Ingrese el ángulo entre su campo magnetico y el vector de
    velocidad de su electron. Este debe estar en el intervalo (0,360].
    CampoB: Ingrese la magnitud del campo magnetico en Teslas. Este 
    debe ser diferente a 0.
    Masa: Ingrese la masa en Kilogramos. Esta debe ser mayor que 0.
    """

    def __init__(self, Energia, Angulo, CampoB, Masa):

        self.q = 1.60217663e-19 #carga de un electrón
        self.Ek = Energia
        self.theta = Angulo
        self.B = CampoB
        self.m = Masa


        #reconocimiento de errores
        try:
            1 / (abs(self.Ek) + self.Ek)
        except:
            print('Ingrese un numero mayor que 0 para la energia cinetica.')
            sys.exit(1)

        try:
            1 / self.theta
        except:
            print('Ingrese un numero para el angulo.')
            sys.exit(1)

        try:
            if self.theta <= 0 or self.theta > 360:
                raise ValueError('Ingrese un angulo en (0,360]')
        except ValueError as e:
            print(f'{e}')
            sys.exit(1)

        try:
            1 / self.B
        except:
            print('Ingrese un numero diferente a 0 para el campo magnetico.')
            sys.exit(1)

        try:
            1 / (abs(self.m) + self.m)
        except:
            print('Ingrese un numero mayor que 0 para la masa.')
            sys.exit(1)

        #corrección de unidades de medida
        self.Ek = self.Ek * self.q
        self.theta = self.theta * np.pi / 180

        self.v = np.sqrt(2 * self.Ek / self.m)
        
        #definimos las componentes verticales y horizontales
        self.vper = self.v * np.sin(self.theta)
        self.vpar = self.v * np.cos(self.theta)

        phi = np.linspace(0, 2 * np.pi, 100)
        t = np.linspace(0, 100, len(phi) * 10)

        #definimos el radio
        self.r = self.m * self.v / (self.q * self.B * np.sin(self.theta))

        #sacamos los arreglos a graficar
        self.X = np.tile(self.r * np.cos(phi), 10)
        self.Y = np.tile(self.r * np.sin(phi), 10)
        self.Z = self.vpar * t

    def figPlot(self):

        #hacemos una gráfica 3d con las listas de coordenadas de cada eje
        fig = plt.figure()
        ax = fig.add_subplot(111, projection= '3d')
        ax.plot(self.X, self.Y, self.Z)
        ax.set_xlabel('X[m]')
        ax.set_ylabel('Y[m]')
        ax.set_zlabel('Z[m]')
        ax.set_title('Trayectoria del electron en B')

        plt.savefig('Trayectoria.png')

