import numpy as np
import matplotlib.pyplot as plt

class ising:
    def __init__(self,N,Tmin,Tmax,ntemp,iteraciones,J=1):
        self.N=N
        self.J=J
        self.temps=np.linspace(Tmin,Tmax,ntemp)
        self.malla = np.random.choice([-1,1],(N,N))
        self.iter=iteraciones

    def mallaRand(self):
        return np.random.choice([-1,1],(self.N,self.N))

    def probabilidad(self,beta):
        return np.exp(-self.energia()/beta)

    def paso(self,beta,malla):
        i = np.random.randint(0,self.N)
        j = np.random.randint(0,self.N)
        s= malla[i,j]
        dE = 2*self.J*s*(malla[(i+1)%self.N,j]+malla[(i-1)%self.N,j]
                         +malla[i,(j+1)%self.N]+malla[i,(j-1)%self.N])
        if dE < 0:
            malla[i,j] *= -1
        elif np.random.rand() < np.exp(-dE/beta):
            malla[i,j] *= -1
        return malla
    
    def magnetizacion(self,malla):
        M = np.sum(malla)
        return M
    
    def cambios(self):
        Mlist=np.zeros(len(self.temps))
        C=np.zeros(len(self.temps))
        for t in range(len(self.temps)):
            beta=1/self.temps[t]
            E=0
            M=0
            E2=0
            for i in range(self.iter):
                mallat=self.paso(beta,self.mallaRand())
                M=self.magnetizacion(mallat)+M
                E=self.energia(mallat)+E
                E2=self.energia(mallat)**2+E2
            C[t] = (E2/(self.iter*self.N**2) - E**2/(self.iter**2*self.N**2))*beta**2
            Mlist[t] = M/(self.iter*self.N**2)

        return C,Mlist
        
    def energia(self,malla):

    
        energy = 0
        for i in range(len(malla)):
            for j in range(len(malla)):
                S = malla[i,j]
                nb = malla[(i+1)%self.N, j] + malla[i,(j+1)%self.N] + malla[(i-1)%self.N, j] + malla[i,(j-1)%self.N]
                energy += -nb*S
        return energy/4
    
    def plotC(self):
        
        tin=self.cambios()
        plt.scatter(self.temps,tin[0])
        plt.title('Calor específico')
        plt.grid()
        plt.xlabel('Temperatura')
        plt.ylabel('C')
        plt.savefig(f'plotC{self.N}')
        plt.close()
        return f'plotC{self.N}'

    def plotM(self):
        tin=self.cambios()
        plt.plot(self.temps,tin[1],'o')
        plt.title('Magnetización')
        plt.grid()
        plt.xlabel('Temperatura')
        plt.ylabel('M')
        plt.savefig(f'plotM{self.N}')
        plt.close()
        return f'plotM{self.N}'
    

    

    
    
    
